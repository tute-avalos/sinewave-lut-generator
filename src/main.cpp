/**
 * @file sine_lut_generator.cpp
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief Genera una tabla de valores para un Look Up Table de una señal
 * senoidal
 * @version 0.4
 * @date 2024-06-04
 *
 * @copyright Copyright (c) 2021-2024
 *
 * Sinewave LUT Generator
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <climits>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#define _exit_with_error(X)                                                    \
  {                                                                            \
    std::cout << (X) << std::endl;                                             \
    std::exit(1);                                                              \
  }

int main(int argc, char *argv[]) {

  constexpr int bytes[]{8, 16, 32};

  if (argc != 3) {
    std::cout << "usage:\n\t " << *argv << " <bits> <steps>" << std::endl;
  } else {
    /****************************** Validaciones ******************************/
    int bits{};
    if (!(std::stringstream(argv[1]) >> bits) || bits <= 0 || bits > 32)
      _exit_with_error(
          "<bits> must be greater than 0 and less or equal than 32.");

    int ib{};
    for (ib = 0; bits > bytes[ib]; ++ib)
      ;

    int entries{};
    if (!(std::stringstream(argv[2]) >> entries) || entries <= 0 ||
        entries > 65535)
      _exit_with_error(
          "<steps> must be greater than 0 and less or equal than 65535.");
    /**************************************************************************/

    /************************* Generación de la tabla *************************/
    // se calculan los pasos para 1 periodo
    const double step{(2.0 * M_PI) / static_cast<double>(entries)};

    // Se imprime el tipo de dato stdint
    std::cout << "uint" << bytes[ib] << "_t sineLUT[] = {";
    std::cout << std::internal
              << std::setfill('0'); // formateo de la salida para rellenar con 0

    double value{};
    const double maxValue{pow(2, bits) - 1};
    // Se calculan los valores de la tabla:
    for (uint16_t i{0}; i < entries; ++i) {
      // cada 8 entradas 1 salto de linea:
      if (!(i % 8))
        std::cout << "\n    ";
      // Se calcula el valor:
      value = ((std::sin(i * step) + 1.0) / 2.0) * (maxValue);

      // Se imprime el valor (el ancho es calculado para la cantidad de bits)
      std::cout << "0x" << std::hex << std::uppercase
                << std::setw(((bits - 1) / 4) + 1)
                << static_cast<uint64_t>(value + 0.5) << ',';
    }
    // Se borra la última coma y se termina la tabla:
    std::cout << "\b \n};" << std::endl;
    /**************************************************************************/
  }
}
